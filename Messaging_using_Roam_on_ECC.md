## Messaging using Roam on ECC

_This article is subject to change_

### Summary

All of the Roam networking protocols that have been built on top of ECC use buffers to receive and store data until a program retrieves the data to interpret it. If we receive data that is destined for a buffer that has not yet been registered then the data is discarded. This prevents the receipt of unwated data.

To send a message using the ECC client you need to first have the buffer for the messaging protocol. Currently that is buffer 0 but this is subject to change.

The registerbuffer rpc call will register a buffer. The only argument for this rpc call is the buffer number that should be registered. It returns a pubkey which is used later to read from the buffer. This is to prevent other programs running on the same machine from reading buffers they are not authorised to read.

To send a packet use the sendpacket rpc call. sendpacket takes 4 arguments, the routingkey of the intended recipient which they should provide to you, the buffer number, the protocol number (usually the same as the buffer number), and the data to be sent. it will return true on successful send or false if something went wrong. A successful send does not mean that the recipient has recieved the packet. It only indicates that we were able to successfully broadcast the data to our peers. 

To retrieve information from a buffer the getbuffer rpc call should be used. But this call requires a signature for authorisation to retrieve the buffer. To get this signature buffersignmessage should be used. buffersignmessage takes 2 arguments, the first is the pubkey returned by registerbuffer. The second is a message that needs to be signed. In order to get a bufers contents the messasage GetBuffer<number><count> should be signed. So for example if it is the first time buffer 0 is being requested the message is GetBufferRequest:01. if it was the 5th time, the message would eb GetBufferRequest:05.

buffersignmessage will return the signature that needs to be passed into getbuffer. getbuffer takes two arguments, the buffer number and the signature to needed to authorise its retrieval.

The buffer that is returned is a array of byte arrays in hexidecmal format. Each byte array is a packet that was received and they are in the order in which they were received.

### Python Example
This is not a working example, it is just to demonstrate the flow of the logic
```
key0 = node0.getroutingpubkey()
key1 = node1.getroutingpubkey()

bufferCount0 = 0
pubkey0 = node0.registerbuffer(0)

bufferCount1 = 0
pubkey1 = node1.registerbuffer(0)

sent = node0.sendpacket(key1, 0, 0, "test string1")

msg = "GetBufferRequest:" + "0" + str((bufferCount_key1 + 1))
sig = node1.buffersignmessage(pubkey1, msg)
buffer = node1.getbuffer(0, sig)
```
As mentioned above, everything in a buffer is a byte array shown in hexidecmal format. The contents of buffer in this example are
```
["7465737420737472696e6731"]
```
